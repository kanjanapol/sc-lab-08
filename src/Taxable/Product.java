package Taxable;

public class Product implements Taxable {
	private String name;
	private double price;

	public Product(String name, double price) {
		this.name = name;
		this.price = price;

	}

	@Override
	public double getTax() {
		double sumTax = 0;
		sumTax = this.price * 0.07;

		return sumTax;
	}

}
