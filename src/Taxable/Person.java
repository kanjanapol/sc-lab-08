package Taxable;

public class Person implements Taxable {
	private String name;
	private double salary;

	// private double sumTax;

	// private double sumTax;
	public Person( ) {
		
		salary = 0 ;
	}
	public Person(String name, double salary_per_yr) {
		this.name = name;
		salary = salary_per_yr;
	}

	@Override
	public double getTax() {
		double sumTax = 0;
		if (salary < 300000) {
			sumTax = (sumTax + (salary * 0.05));

		}
		else
		if (salary >= 300000) {
			sumTax = (sumTax + (300000 * 0.05));
			// s = salary-300000;

		}
		if (salary > 300000 && salary <= 500000) {
			sumTax = (sumTax + ((salary - 300000) * 0.1));

		}
		if (salary > 500000 && salary <= 750000) {

			sumTax = (sumTax + ((salary - 500000) * 0.15));

		}
		

		return sumTax;
	}

}
