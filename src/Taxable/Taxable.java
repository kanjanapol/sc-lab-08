package Taxable;

public interface Taxable {
	
	double getTax();
}
