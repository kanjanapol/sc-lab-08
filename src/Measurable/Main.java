package Measurable;

public class Main {
		public static void main(String[] args){
			Measurable [] person = new Measurable[5];
			person[0] = new Person("Aar",155);
			person[1] = new Person("Mon",145);
			person[2]= new Person("Non",168);
			person[3] = new Person("x",170);
			person[4] = new Person("Koh",175);
		    
			double avg_high = Data.average(person) ;
			System.out.println(avg_high);
			Measurable  compare_1 = new Person(avg_high);
			
			Measurable [] countries = new Measurable[5];
			countries[0] = new Country("Japan",1238);
			countries[1] = new Country("Thailand",456);
			countries[2] = new Country("Eng",8567987);
			countries[3] = new Country("Austria",987234);
			countries[4] = new Country("Brazil",7654);
			
			double avg_area = Data.average(countries) ;
			System.out.println("Average of area : " +avg_area);
			Measurable  compare_2 = new Country(avg_area);
			
			Measurable [] account = new Measurable[5];
			account[0]  = new BankAccount(0);
			account[1]  = new BankAccount(100);
			account[2]  = new BankAccount(230);
			account[3]  = new BankAccount(34560);
			account[4]  = new BankAccount(12000);
			
			double avg_account = Data.average(account) ;
			System.out.println("Average of money in account : " +avg_account); 
			Measurable compare_3 = new BankAccount(avg_account);
			
			Measurable min = Data.min(compare_1,compare_2);
			Double a = min.getMeasure();
			System.out.println("Average of persons : " +a+ " less than average' s area : "+compare_2.getMeasure());
			
			Measurable min2 = Data.min(compare_3, compare_1);
			Double b = min2.getMeasure();
			System.out.println("Average of persons : " +b+ " less than  average' s accounts: "+compare_3.getMeasure());
			
			Measurable min3 = Data.min(compare_3, compare_2);
			Double c = min3.getMeasure();
			System.out.println("Average of accounts : " +c+ " less than  average' s area: "+compare_2.getMeasure());
			
		}
}
